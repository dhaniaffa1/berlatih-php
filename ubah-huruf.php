<?php
function ubah_huruf($text){

$textAbjad = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
$array = [];
$panjang = strlen($text);

for($i = 0;$i<$panjang;$i++){
    $find = array_search($text[$i],$textAbjad);
    $hasil = $textAbjad[$find+1];
    array_push($array,$hasil);
}

foreach($array as $kata){
    echo $kata;
}
echo "<br>";
}
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu


?>