<?php
echo "<h1>SOAL 1</h1>";
function tentukan_nilai(int $angka){
    if($angka>= 85 && $angka <=100){
        return "Nilai $angka : Sangat Baik";
    } elseif($angka >= 70 && $angka < 84){
        return "Nilai $angka : Baik";
    } elseif ($angka >= 60 && $angka < 70){
        return "Nilai $angka : Cukup";
    } else{
        return "Nilai $angka : Kurang";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";    
echo tentukan_nilai(76); //Baik
echo "<br>";    
echo tentukan_nilai(67); //Cukup
echo "<br>";    
echo tentukan_nilai(43); //Kurang
