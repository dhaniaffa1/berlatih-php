<?php

function tukar_besar_kecil($kata){
    $panjang = strlen($kata);
    $array = [];
    for($i=0;$i<$panjang;$i++){
        if (ctype_upper($kata[$i])==1){
            array_push($array,strtolower($kata[$i]));
        } else {
            array_push($array,strtoupper($kata[$i]));
    
        }
    }
    foreach($array as $word){
        echo $word;
    }
    echo "<br>";
}
// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>